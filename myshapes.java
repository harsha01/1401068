import java.util.*;
import java.io.*;
import java.lang.*;

class Shape
{
	Scanner S = new Scanner(System.in);
	String colour;
	
	Shape() // constructor
	{
		colour = null;
	}
	
	public void get_colour()
	{
		try  // for creating file
		{
			FileOutputStream Fout = new FileOutputStream("filename");
			ObjectOutputStream Oout = new ObjectOutputStream(Fout);
			
			String co = " The Colour Is :" +colour; // entering details into the file
			byte[] c = co.getBytes();
			Oout.writeObject(c);
			
		}
		
		catch(Exception e) // Ecxeption
		{
			System.out.println("Error...");
		}
	}
	
	protected String getValueOf(double hight_r) { // set datatype of the variable
		// TODO Auto-generated method stub
		return null;
	}

	public void set_colour() // setting colour
	{
		FileInputStream Fin;
		ObjectInputStream Oin; 
		try  // 
		{
			Fin = new FileInputStream("filename");
			Oin = new ObjectInputStream(Fin);
			
			int value = 0;
			
			while(Fin.read() != -1) //reading file
			{
				System.out.println((char)value);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error...");
		}
	}
	
	public void get_details_colour() // showing colour details
	{
		System.out.println("Enter The Colour You Want:");
		colour = S.next();
	}
}

abstract class Shape_2D extends Shape // abstract 2D class
{
	double area;
	
	Shape_2D() // constructor
	{
		area = 0;
	}
	
    abstract void  find_area();
	abstract void get_vertex(); // abstract functions
}

abstract class Shape_3D extends Shape // abstract 3D class
{
	double volume;
	double surfacearea;
	
	Shape_3D() // constructor
	{
		volume = 0;
		surfacearea = 0;
	}
	
	abstract void find_surface(); // abstract function
    abstract void find_volume();
}

class Rectangle extends Shape_2D
{
	Scanner S = new Scanner(System.in);
	double a,b,c,d,a1,b1,a2,b2,a3,b3,a4,b4;
	double hight_r;
	double width_r;
	
	Rectangle()
	{
		a = 0;b = 0;
		c = 0;d = 0;
	}
	
	public void create_file_r() // creating file
	{
		try // entering details
		{
			FileOutputStream Fout = new FileOutputStream("filename");
			ObjectOutputStream Oout = new ObjectOutputStream(Fout);
			
			String a1 = " The Value Of Length Of Rectangle Is  : "+getValueOf(hight_r);
			byte[] A = a1.getBytes();
			Oout.writeObject(A);

            String b1 = " The Value Of Width Of Rectangle Is  :" +getValueOf(width_r);
			byte[] B = b1.getBytes();
			Oout.writeObject(B);
			
			String c1 = " The Area Of Rectangle Is  :" +getValueOf(area);
			byte[] C= c1.getBytes();
			Oout.writeObject(C);
			
			String d1 = " The Area Of Rectangle Is  :" +colour;
			byte[] D= d1.getBytes();
			Oout.writeObject(C);

		}
		
		catch(Exception e) // exception
		{
			System.out.println("Error...");
		}
	}
	
    public void set_rectangle() // showing details of rectangle for rectangle
	{
		try
		{
			FileInputStream Fin = new FileInputStream("filename");
			ObjectInputStream Oin = new ObjectInputStream(Fin);
			
			int value = 0;
			
			while( Fin.read() != -1) //reading file
			{
				System.out.println((char)value);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error...");
		}
	}
	
		public void get_vertices_r() // taking vertices
		{
			System.out.println("This Program Will Take the Co-ordinates Of Vertices Of The Rectangle...");
			
			System.out.println("Enter Co-ordinates Of First Vertices :");
			a1 = S.nextInt();
			b1 = S.nextInt();
			
			System.out.println("Enter Co-ordinates Of Second Vertices(Hight) :");
			a2 = S.nextInt();
			b2 = S.nextInt();
			
			System.out.println("Enter Co-ordinates Of Third Vertices(Width) :");
			a3 = S.nextInt();
			b3 = S.nextInt();
			
			System.out.println("Enter Co-ordinates Of Fourth Vertices(Other) :");
			a4 = S.nextInt();
			b4 = S.nextInt();
			
		}
		
		public void find_area() // finding area
		{
			hight_r = Math.sqrt((a2 - a1)*(a2 - a1) + (b2 - b1)*(b2 - b1));
		    width_r = Math.sqrt((a3 - a1)*(a3 - a1) + (b3 - b1)*(b3 - b1));
			
			area = (hight_r * width_r);
            
            System.out.println("The Area Of Rectangle Is :" +area);			
		}

		private double sqrt(int i) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		void get_vertex() {
			// TODO Auto-generated method stub
			
		}
		
}

class Circle extends Shape_2D //  circle class
{
	Scanner S = new Scanner(System.in);
	double x1,x2,y1,y2;
	double radius_ci;
	
	Circle() // constructor
	{
		x1 = 0;x2 = 0;
		y1 = 0;y2 = 0;
	}
	
	public void create_file_ci() //crteation of file
	{
		try
		{
			FileOutputStream Fout = new FileOutputStream("filename");//creation of binary
			ObjectOutputStream Oout = new ObjectOutputStream(Fout);
			
			String a1 = " The Value Of radius Of Circle Is  :" +getValueOf(radius_ci);
			byte[] A = a1.getBytes();//radius of circle
			Oout.writeObject(A);

			String b1 = " The Colour Of Rectangle Is  :" +colour;//colour
			byte[] B= b1.getBytes();
			Oout.writeObject(B);
			
            String c1 = " The Area Of Rectangle Is  :" +getValueOf(area);//area
			byte[] C= c1.getBytes();
			Oout.writeObject(C);

		}
		
		catch(Exception e)
		{
			System.out.println("Error...");//if entry is incorrect
		}
	}
	
	public void set_circle()//taking the data of circle
	{
		try
		{
			FileInputStream Fin = new FileInputStream("filename");//file
			ObjectInputStream Oin = new ObjectInputStream(Fin);
			
			int value = 0;
			
			while( Fin.read() != -1)//reading the file
			{
				System.out.println((char)value);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error...");//if found incorrect
		}
	}
	
	public void get_vertices_ci()//the circle data
	{
		System.out.println("Enter The Co-ordinates Of The Centre Of The Circle :");
		x1 = S.nextInt();//center's co-ordibnates
		y1 = S.nextInt();
		
		System.out.println("Enter The Co-ordinates Of The Point From The Surface Of The Circle :");
		x2 = S.nextInt();//the point on the circumference means the radius
		y2 = S.nextInt();
		
	}
	
	public void find_area()//function of area
	{		
		radius_ci = Math.sqrt((x2 - x1)*(x2 - x1) +( y2 - y1)*( y2 - y1));//using math and distance formula
		
		area = (float) ((3.14) * (radius_ci*radius_ci));//area of the circle
		
		System.out.println("The Area Of The Circle Is :" +area);//shwowing the area
	}

	private double sqrt(int i) //for math integer
	{

		return 0;
	}

	@Override
	void get_vertex() {
		// TODO Auto-generated method stub
		
	}
	
}

class Cuboid extends Shape_3D//cuboid class
{
	double p1,p2,p3,p4,q1,q2,q3,q4,r1,r2,r3,r4;//variable declaration
	double length_cu;
	double width_cu;
	double hight_cu;
	
	Cuboid()//constructor
	{
		p1 =0;q1 =0;r1 = 0;//intialization of all the variables to 0
		p2 = 0;q2 = 0;r2 = 0;
		p3 = 0;q3 = 0;r3 = 0;
		p4 = 0;q4 = 0;r4 = 0;
	}
	
	public void create_file_cu()//creating file of cuboid
	{
		try
		{
			FileOutputStream Fout = new FileOutputStream("filename");//binary file creation
			ObjectOutputStream Oout = new ObjectOutputStream(Fout);
			
			String a1 = " The Value Of Length Of Cuboid Is  :" +getValueOf(length_cu);
			byte[] A = a1.getBytes();//lenght
			Oout.writeObject(A);

            String b1 = " The Value Of Width Of Cuboid Is  :" +getValueOf(width_cu);
			byte[] B = b1.getBytes();
			Oout.writeObject(B);//width
			
			String d1 = " The Value Of Hight Of Cuboid Is  :" +getValueOf(hight_cu);
			byte[] D = d1.getBytes();//height
			Oout.writeObject(D);
			
			String c1 = " The Surface Area Of Cuboid Is  :" +getValueOf(surfacearea);
			byte[] C= c1.getBytes();//surface area
			Oout.writeObject(C);
			
			String e1 = " The Volume Of Cuboid Is  :" +getValueOf(volume);
			byte[] E= e1.getBytes();//volume
			Oout.writeObject(E);
			
			String f1 = " The Colour Of Cuboid Is  :" +colour;
			byte[] F= f1.getBytes();//colour
			Oout.writeObject(F);

		}
		
		catch(Exception e)
		{
			System.out.println("Error...");//if entered incorrect
		}
	}
	
	public void set_cuboid()//setting cuboid
	{
		try
		{
			FileInputStream Fin = new FileInputStream("filename");
			ObjectInputStream Oin = new ObjectInputStream(Fin);
			
			int value = 0;
			
			while(Fin.read() != -1)//reading file
			{
				System.out.println((char)value);
			}
		}
		catch(Exception e)
		{
			System.out.println("Error...");
		}
	}
	
	public void get_vertices_cu() 
	{//taking dimensions of cuboid
		System.out.println("Enter The Three Co-ordinates Of First point :");//taking the co-ordinates of it
	    p1 = S.nextInt();
		q1 = S.nextInt();
		r1 = S.nextInt();
		
		System.out.println("Enter The Co-ordinates of First Dimension Point(Length) :");
	    p2 = S.nextInt();
		q2 = S.nextInt();
		r2 = S.nextInt();
		
		System.out.println("Enter The Co-ordinates Of Second Dimension Point(Width) :");
	    p3 = S.nextInt();
		q3 = S.nextInt();
		r3 = S.nextInt();
		
		System.out.println("Enter The Co-ordinates Of Third Dimension Point(Hight) :");
	    p4 = S.nextInt();
		q4 = S.nextInt();
		r4 = S.nextInt();
		
	}
	
	public void find_surface()//for surface area
	{
	
		double length_cu = Math.sqrt((p2 - p1)*(p2 - p1) + (q2 - q1)*(q2 - q1) + (r2 - r1)*(r2 - r1));
		double width_cu = Math.sqrt((p3 - p1)*(p3 - p1) + (q3 - q1)*(q3 - q1) + (r3 - r1)*(r3 - r1));
		double hight_cu = Math.sqrt((p4 - p1)*(p4 - p1) + (q4 - q1)*(q4 - q1) + (r4 - r1)*(r4 - r1));
		
		surfacearea = 2*length_cu + 2*hight_cu + 2*width_cu;//calculating the suface area
		
		System.out.println("The Surface Area Of This Cuboid Is : " +surfacearea);//showing the surface area
	}
	
	private float sqrt(int i) {//for math
		// TODO Auto-generated method stub
		return 0;
	}

	public void find_volume()//volume
	{
		double length_cu = Math.sqrt((p2 - p1)*(p2 - p1) + (q2 - q1)*(q2 - q1) + (r2 - r1)*(r2 - r1));
		double width_cu = Math.sqrt((p3 - p1)*(p3 - p1) + (q3 - q1)*(q3 - q1) + (r3 - r1)*(r3 - r1));
		double hight_cu = Math.sqrt((p4 - p1)*(p4 - p1) + (q4 - q1)*(q4 - q1) + (r4 - r1)*(r4 - r1));
		
		double volume = (length_cu)*(width_cu)*(hight_cu);//calculating the volumes
		System.out.println("The Volume Of This Cuboid Is :" +volume);//showing the volumes
	}
}

class Sphere extends Shape_3D//the sphere class
{
	Scanner S = new Scanner(System.in);//scanner function
	int x1,x2,z1,z2,y1,y2;//variables
    double radius_s;//radius
	Sphere()//constructor
	{
	   x1 = 0;x2 = 0;//intializing the value to 0
	   y1 = 0;y2 = 0;//all are co-ordinates
	   z1 = 0;z2 = 0;
	}
	
	public void create_file_s()//file creation
	{
		try
		{
			FileOutputStream Fout = new FileOutputStream("filename");//for binary
			ObjectOutputStream Oout = new ObjectOutputStream(Fout);
			
			String a1 = " The Value Of Radius Of Sphere Is  :" +getValueOf(radius_s);
			byte[] A = a1.getBytes();
			Oout.writeObject(A);

            String c1 = " The Surface Area Of Sphere Is  :" +getValueOf(surfacearea);
			byte[] C= c1.getBytes();
			Oout.writeObject(C);
			
			String b1 = " The Colour Sphere Is  :" +colour;
			byte[] B= b1.getBytes();
			Oout.writeObject(B);
			
			String d1 = " The Volume Sphere Is  :" +getValueOf(volume);
			byte[] D= d1.getBytes();
			Oout.writeObject(D);

		}
		
		catch(Exception e)
		{
			System.out.println("Error...");
		}
	}
	
	public void set_sphere()//reading the set values
	{
		try
		{
			FileInputStream Fin = new FileInputStream("filename");
			ObjectInputStream Oin = new ObjectInputStream(Fin);
			
			int value = 0;
			
			while(Fin.read() != -1)//reading the file
			{
				System.out.println((char)value);
				value++;
			}
			Fin.close();
		}
		catch(Exception e)
		{
			System.out.println("Error...");
		}
	}
	
	public void get_vertices_s()//takin the all dimensions
	{
		System.out.println("Enter Co-ordinates Of The Centre Of The Sphere :");
		x1 = S.nextInt();//circle
		y1 = S.nextInt();
		z1 = S.nextInt();
		
		System.out.println("Enter The Co-ordinates Of Any Point On The Surface Of The Sphere(Radius) :");
		x2 = S.nextInt();//point on surface of the sphere
		y2 = S.nextInt();
		z2 = S.nextInt();
	}
	
	public void find_surface()//area
	{
		radius_s = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) + (z2 - z1)*(z2 - z1));
		
		surfacearea = (float) (4*(3.14)*(radius_s)*(radius_s));//calulating the surface area
		
		System.out.println("The Surface Area Of The Sphere Is :" +surfacearea);//showing the surface area
	}
	
	private float sqrt(int i) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void find_volume()//volume
	{
		radius_s = sqrt((x2 - x1)*(x2 - x1) + (y2 - y1)*(y2 - y1) + (z2 - z1)*(z2 - z1));
		
		volume = (float) ((4/3)*(3.14)*(radius_s)*(radius_s)*(radius_s));//calculating the volume
		
		System.out.println("Volume Of The Sphere Is :" +volume);//showing the volume
	}
}


public class myshapes//showing the main class
	{
	public static void main(String arg[])
	{
		Scanner S = new Scanner(System.in);//scanner function
		Shape SA = new Shape();//creating the object of the shape class
		Shape_3D S3D = null ;//object of 3_d class
		Shape_2D S2D = null;//object of 2_d class
		//for runtime polumorphism
		
		System.out.println("Which Dimension Do You Want To Choose ?");//now giving the choices tpo the user
		System.out.println("1. 2D");
		System.out.println("2. 3D");
		System.out.println("3. Exit");
		int f = S.nextInt();//taking choice
		do
		{
			if(f == 1)//if pressed 1
			{
				SA = S2D;//now 2_d object is used
				
				System.out.println("Choose Any One Shape Of Which You Want To Find Area :");
				System.out.println("1. Rectangle");//giving choices
				System.out.println("2. Circle ");
				int choose = S.nextInt();//taking choice
				
				if(choose == 1)//if chosen 1 for rectangle
				{
					Rectangle R = new Rectangle();//object of rectangle is created
					S2D = R;//now the s2d(object of 2_d) is rectangle
					
					R.get_details_colour();//now the upper declared functions are executed
					R.get_vertices_r();//will take the all date= from the user and display
					R.find_area();
					R.create_file_r();//a file of the rectangle is created
					R.set_rectangle();
				}
				
				else if(choose == 2)//choosen circle
				{
					Circle CI = new Circle();//circle object is created
					S2D = CI;
					
					CI.get_details_colour();//taking the the data of the circle
					CI.get_vertices_ci();
					CI.find_area();
					CI.create_file_ci();//created the file of circle
				
					CI.set_circle();//now the setting the circle information
				}
			}
			
			else if(f == 2)//if choosen the 2 for 3_d
			{
				
				SA = S3D;//object of 3_d is taken
				
				System.out.println("Choose Any One Shape Of Which You Want To Find Surface Area And Volume :");
				System.out.println("1. Cuboid");//giving choice to the object
				System.out.println("2. Sphere ");
				int choose = S.nextInt();
				
				if(choose == 1)//if choosen cuboid
				{
					Cuboid CU = new Cuboid();//object of cuboid is created
					S3D = CU;
					
					CU.get_details_colour();//taking the cuboid data
					CU.get_vertices_cu();
					CU.find_surface();
					CU.find_volume();
					CU.create_file_cu();//file is created of cuboid
					CU.set_cuboid();
				}
				
				else if(choose == 2)//if choosen sphere
				{
					Sphere SP = new Sphere();//object of sphere is created
					S3D = SP;
					
					SP.get_details_colour();//taking the data of sphere with the created  functions
					SP.get_vertices_s();
					SP.find_surface();
					SP.find_volume();
					SP.create_file_s();//file is created of tyhe sphere
					SP.set_sphere();
				}
			}
		}while(f < 3);//end of do-while loop
	}
}//end
