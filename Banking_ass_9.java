/*Assingment-9 Q-1*/

import java.util.*;
import java.io.*;

abstract class Account											/*Starting of Abstract class*/
{
	Scanner sc=new Scanner(System.in);

	protected double Acc_no;
	protected double Balance;
	protected int Month,Year,C_Month,C_Year;

	abstract public void Create();								/*abstract Member function*/

	abstract public void Detail();								/*abstract Member function*/
	
	public void Deposit() throws MyException 
	{
		System.out.print("Enter the amount you want to deposite:");
		float i=sc.nextFloat();
		if(i<1)
		{
			throw new MyException();
		}

		Balance=Balance+i; 

		System.out.println("Deposit Sucessful...");
		System.out.println("Balance :"+Balance); 
	}
	
	abstract public void Withdraw() throws MyException;								/*abstract Member function*/

	abstract public void Interest();

	abstract public void file();									/*abstract Member function*/

	abstract public void delete();
}

 															/*Ending of abstract class*/

class Saving extends Account										/*starting of Saving class & inherite Account class*/
{
	Scanner sc=new Scanner(System.in);
	float I_rate;
	double Interest=0;
	public void Create()										/*Create Member function Saving Account*/
	{
		do{
		System.out.print("Enter Acc No. :");
		Acc_no=sc.nextDouble();
		if(Acc_no<=0)											/*if condition to check Account number*/
		{
			System.out.println("Account Number can not be negative..");
		}
		}while(Acc_no<=0);										/*do while loop*/

		do{
		System.out.print("Enter Intrestrate :");
		I_rate=sc.nextFloat();
		if(I_rate<=0)											/*if condition to check Intrest*/
		{
			System.out.println("Intrstrate can not be negative..");
		}
		}while(I_rate<=0);										/*do while loop*/

		do{
		System.out.print("Enter Month(Eg:January-->1) :");
		Month=sc.nextInt();
		if(Month<=0)											/*if condition to check month*/
		{
			System.out.println("Month can not be negative..");
		}
		}while(Month<=0);										/*do while loop*/

		do{
		System.out.print("Enter Year :");
		Year=sc.nextInt();
		if(Year<=0)											/*if condition to check year*/
		{
			System.out.println("Year can not be negative..");
		}
		}while(Year<=0);										/*do while loop*/

		System.out.print("Enter Balance :");
		Balance=sc.nextLong();
		if(Balance>999)										/*if condition to check Balance*/
		{
			System.out.println("Account created...");
			System.out.println("Press-->2 to continue...");
		}
		else
		{		
			System.out.println("Press-->1 & put minimum Balance 1000/-");
		}
	}													
		
	public void Detail()										/*Detail member function for print detail*/
	{
		System.out.println("Account no. :"+Acc_no);
		System.out.println("Balance     :"+Balance);
		System.out.println("Intrst Rate :"+I_rate);
		System.out.println("Date & Year when Account created :"+Month+"/"+Year);
	}

 	
	public void Withdraw() throws MyException									/*Withdraw member function*/
	{
		System.out.print("Enter Amount you want to withdraw :");
		float j=sc.nextFloat();
		if(j<1)
		{
			throw new MyException();
		}

		if(j<Balance-500)										/*if condition for minimum 500/- balance*/
		{
			System.out.println("withdraw Succesfull....");
			Balance=Balance-j;
			System.out.println("Balance :"+Balance);
		}
		else
		{	
			System.out.println("Withdrawal Unsucessfull...");
			System.out.println("Minimum balance needed 500/-");
		}

	}

	public void Interest()										/*Member function for Intrest*/
	{
		do{
		System.out.print("Enter Current Month(Eg:January-->1) :");
		C_Month=sc.nextInt();
		if(C_Month<=0)											/*if condition to check month*/
		{
			System.out.println("Month can not be negative..");
		}
		}while(C_Month<=0);										/*do while loop*/

		do{
		System.out.print("Enter Current Year :");
		C_Year=sc.nextInt();
		if(C_Year<=0)											/*if condition to check Year*/
		{
			System.out.println("Year can not be negative..");
		}
		}while(C_Year<=0);										/*do while loop*/

		if(Year<=C_Year)										/*if  condition for checking year*/
		{
			
			int M=C_Month-Month;
			int Y=C_Year-Year;

			int T=M+(Y*12);
			System.out.println("No. of Months :"+T);
			int e=T/3;
			for(int g=0;g<e;g++)
			{
				Interest=(Balance*I_rate*(0.25))/100;
				System.out.println("Intrest-"+(g+1)+" :"+Interest);
				Balance=Balance+Interest;	
			}
		}
		
		System.out.println("Balance :"+Balance);
	}

	public void file()
	{
		String fileName=String.valueOf(Acc_no);	

		FileOutputStream outs=null;
		ObjectOutputStream oout = null;		
		
		try
		{
			outs=new FileOutputStream(fileName);
			oout=new ObjectOutputStream(outs);

			String ac="Acc_no :"+Acc_no+"\n";
			String ba="Balance :"+Balance+"\n";
			String ir="Interest :"+I_rate+"\n";
			String ma="Month :"+Month+"\n";
			String ya="Year :"+Year+"\n";

			byte[] a=ac.getBytes();
			byte[] b=ba.getBytes();
			byte[] i=ir.getBytes();
			byte[] m=ma.getBytes();
			byte[] y=ya.getBytes();

			oout.writeObject(a);
			oout.writeObject(b);
			oout.writeObject(i);
			oout.writeObject(m);
			oout.writeObject(y);

			System.out.println("Data stored...");
		}
		catch(IOException ex)
		{	
		
			System.out.println("Error while writting file :"+fileName);
		}

		finally
		{
			try
			{
				oout.close();
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}
	}

	public void delete()									/*Member function for delete file*/ 
	{
		try
		{
			System.out.print("Enter File Name :");
			String xyz=sc.next();
		
			File file=new File(xyz);							/*Convert Account Number in String*/

			if(file.delete())								/*Delete file*/
			{
				System.out.println("File deleted...");
			}
			else
			{
				System.out.println("Delete operation is failed...");
			}
		}
		catch(Exception e)
		{
			System.out.println("Error :"+e);
		}
	}
}												/*Ending of Saving class*/

class MyException extends Exception					/*My own Exception*/
{
	MyException ()
	{}

	public String toString()
	{
		return "Amount is less than 1....";
	}
	
}

class Current extends Account										/*Starting of Current class & inherite Account class*/
{
	Scanner sc=new Scanner(System.in);
	float k;

	public void Create()										/*Create member function for current Account*/
	{
		do{
		System.out.print("Enter Acc No. :");
		Acc_no=sc.nextDouble();
		if(Acc_no<=0)											/*if condition to check Account number*/
		{
			System.out.println("Account Number can not be negative..");
		}
		else
		continue;
		}while(Acc_no<=0);										/*do while loop*/

		do{
		System.out.print("Enter Balance :");
		Balance=sc.nextLong();
		if(Balance<=0)											/*if condition to check balancce*/
		{
			System.out.println("Balance can not be negative..");
		}
		}while(Balance<=0);										/*do while loop*/

		do{
		System.out.print("Enter overdraft limit :");
		k=sc.nextFloat();
		if(k<=0)												/*if condition to check Overdraft limit*/
		{
			System.out.println("Over draftlimit can not be negative..");
		}
		}while(k<=0);											/*do while loop*/
		
		System.out.println("Press-->2 to continue...");
	}

	public void Detail()										/*Detail member function to print Account detail*/			
	{
		System.out.println("Account no. :"+Acc_no);
		System.out.println("Balance     :"+Balance);
		System.out.println("Overdraft limit :"+k);
	}

	
	public void Withdraw() throws MyException								/*Withdraw Member function to withdraw money*/
	{
		System.out.print("Enter Amount you want to withdraw :");
		float l=sc.nextFloat();
		if(l<1)
		{
			throw new MyException();
		}

		if(l<Balance+k)
		{
			System.out.println("withdraw Succesfull....");
			Balance=Balance-l;								
			System.out.println("Balance :"+Balance);	
		}
		else
		{
			System.out.println("Overdraft limit reached....");
		}
	}

	public void Interest()					
	{
		System.out.println("Sorry,this is Current Account"); 
	}

	public void  file()											/*Member function file which binary create file*/
	{
		String fileName=String.valueOf(Acc_no);	
		
		FileOutputStream outs=null;
		ObjectOutputStream oout=null;

		try
		{
			outs=new FileOutputStream(fileName);
			oout=new ObjectOutputStream(outs);			/*Create binary file*/

			String ac="Acc_no :"+Acc_no+"\n";
			String ba="Balance :"+Balance+"\n";
			String ov="Overdraft limit :"+k+"\n";

			byte[] a=ac.getBytes();
			byte[] b=ba.getBytes();
			byte[] o=ov.getBytes();

			oout.writeObject(a);
			oout.writeObject(b);
			oout.writeObject(o);

			oout.close();
			System.out.println("Data stored...");
		}
		catch(IOException ex)
		{		
			System.out.println("Error while writting file :"+fileName);
		}
		finally
		{
			try
			{
				oout.close();
			}
			catch(Exception e)
			{
				System.out.println(e);
			}
		}

	}														/*Ending of file function*/


	public void delete()										/*Member function to delete file*/
	{

		try
		{
			File file=new File(String.valueOf(Acc_no));

			if(file.delete())
			{
				System.out.println("File deleted...");
			}
			else
			{
				System.out.println("Delete operation is failed...");
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}	
}														/*Ending of Current Account class*/

class Banking_ass_9												/*Strating of Main class*/
{
	public static void main(String[] args)						/*Main Function*/
	{
		Scanner sc=new Scanner(System.in);

		Account A1;										/*Refrence of Account class*/
		
		int c;
		do{
		   System.out.println("Enter the type of Account you want to create");
		   System.out.println("1-->Savings Account");
		   System.out.println("2-->Current Account");
		   int s=sc.nextInt();

	        if(s==1)
		   {
			Saving S1=new Saving();
			A1=S1;										/*A1 get refrence of Saving class(Run time polymophisms)*/
			A1.Create();
			A1.file();
		   }
		   else
		   {
			Current C1=new Current();
			A1=C1;										/*A1 get refrence of Current class(Run time polymorphisms)*/
			A1.Create();
			A1.file();
		   }
		   c=sc.nextInt();
		  }while(c==1);

		int r;
		do{
			System.out.println("--------------------------------------------------");
			System.out.println("0-->Exit");
			System.out.println("1-->Account Detail");
			System.out.println("2-->Deposit money");
			System.out.println("3-->Withdraw money");
			System.out.println("4-->Count  Interest");
			System.out.println("5-->Delete data");
			System.out.print("\nSelect :");
			r=sc.nextInt();
			System.out.println("--------------------------------------------------");

			switch(r)										/*Switch case to perform different task*/
			{

				case 1:									/*to show account detail*/
					A1.Detail();
					break;

				case 2:
					try
					{
						A1.Deposit();							/*to deposit money*/
					}
					catch(MyException e)
					{
						System.out.println("Error :"+e);
					}
					A1.file();
					break;

				case 3:
					try
					{
					A1.Withdraw();							/*to withdraw money*/
					}
					catch(MyException e)
					{
						System.out.println("Error :"+e);
					}							
					A1.file();					
					break;

				case 4:
					A1.Interest();							/*to get Interest*/
					A1.file();
					break;
				
				case 5:
					A1.delete();							/*to delete file*/
					break;

			}
		}while(r!=0);
	} 													/*Ending of Main function*/
}														/*Ending of Main class-*/
