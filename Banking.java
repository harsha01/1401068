/*Assingment-8*/

import java.util.*;
import java.io.*;

abstract class Account //making class account for user detrails.
{
	Scanner sc=new Scanner(System.in); //scanner function

	protected String Acc_no;
	protected float Balance;
	protected int count=0;
	abstract public void Create();

	public void Detail()
	{
		System.out.println("Account no. :"+Acc_no);
		System.out.println("Balance     :"+Balance);
	}

	public int count()
	{
		count++;
		return count;
	}

	public void Deposit() //making deposit class for the user to deposit the amount
	{
		System.out.print("Amount you want to deposit:");
		float i=sc.nextFloat();   //taking the deposit money 

		Balance=Balance+i;       //change in the balance

		System.out.println("Deposit Sucessful..."); 
		System.out.println("Balance :"+Balance);   //for printing the balance
	}
	
	abstract public void Withdraw();   //making functions

	abstract public void Interest();
	
	abstract public void file();
	
	abstract public void delete();
	
}

class Saving extends Account //making saving class
{
	Scanner sc=new Scanner(System.in); //scanner function
	float I_rate;
	float Interest=0;
	public void Create()  //taking details for saving acc
	{
		System.out.print("Enter Acc No. :");
		Acc_no=sc.nextLine();
		System.out.print("Enter Intrest rate :");
		I_rate=sc.nextFloat();
		System.out.print("Enter Balance :");
		Balance=sc.nextLong();
		if(Balance>=1000)   //minimum 1000/- setting
		{
			System.out.println("Account created...");
			System.out.println("Press 2 to continue...");
		}
		else
		{		
			System.out.println("Press 1 & put minimum Balance 1000/-");
		}
	}
		
 	
	public void Withdraw() //the function to withdraw the money
	{
		System.out.print("Enter Amount you want to withdraw :");
		float j=sc.nextFloat(); //taking the amount to be withdraw

		if(j<Balance-500)//putting a limit on the balance
		{
			System.out.println("withdraw Succesfull....");
			Balance=Balance-j;                                 //subtracting the withdrawing money from the balance
			System.out.println("Balance :"+Balance);//showing the current balance
		}
		else
		{	
			System.out.println("Withdrawal Unsucessfull...");
			System.out.println("Minimum balance needed 500/-");//putting a limit on the balance
		}

	}

	public void Interest()//function interest to calculate the interest
	{
		int t_month,t_year,year,month;
		Scanner sc=new Scanner(System.in);
		System.out.println("enter the todays month: ");//taking the current month
		t_month=sc.nextInt();
		System.out.println("enter the current year: ");//current year
		t_year=sc.nextInt();
		System.out.println("depository month: "); //depository month
		month=sc.nextInt();
		System.out.println("depository year: ");  //depository year
		year=sc.nextInt();
		if(year<=t_year)
		{
			int q=t_month-month;             //calculation total months
			int w=t_year-year;              //calculating total year
			int m=q+(w*12);                //total calculation of months
			System.out.println("months: "+m);
			int p=m/3;                    //dividing by 3 to get number of quaters
			for(int h=0;h<p;h++)
			{
			Interest = (Balance*I_rate*(0.25f))/100;  //calculating the interest
			System.out.println("interest: " +(h+1)+" : " +Interest);
			Balance= Balance +Interest;  //changing the balance by adding the interest
			}
		}
	}
	public void file()//creating file
	{
		String fileName=String.valueOf(Acc_no);	//file with account number

		try
		{
			FileOutputStream outs=new FileOutputStream(fileName);
			ObjectOutputStream oout=new ObjectOutputStream(outs);//binnary file
			String ac="Acc_no :"+Acc_no+"\n";
			String ba="Balance :"+Balance+"\n";
			String ir="Interest :"+I_rate+"\n";
			
			byte[] a=ac.getBytes();
			byte[] b=ba.getBytes();
			byte[] i=ir.getBytes();
			

			oout.writeObject(a);
			oout.writeObject(b);
			oout.writeObject(i);
		

			oout.close();

			System.out.println("Data stored");
		}
		catch(IOException ex)
		{
			System.out.println("Error while writting file :"+fileName);
		}
	}
	public void delete() //deleting the file
	{
		try
		{
			File file=new File(String.valueOf(Acc_no));//passing the account number

			if(file.delete())						
			{
				System.out.println("File deleted");//successful deletion
			}
			else
			{
				System.out.println("Delete operation is failed");//incorrect
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}


}

class Current extends Account //current account
{
	Scanner sc=new Scanner(System.in);
	float k;

	public void Create() //now taking or alloting the account number to user for cuurrent
	{
		System.out.print("Enter Acc No. :");
		Acc_no=sc.nextLine();
		System.out.print("Enter Balance :"); //taking the balance means the deposite money at the opening
		Balance=sc.nextLong();
		System.out.print("Enter overdraft limit :");//setting the overdraft limit
		k=sc.nextFloat();	
		System.out.println("Press 2 to continue...");
	}

	
	
	public void Withdraw() //withdraw function
	{
		System.out.print("Enter Amount you want to withdraw :");
		float l=sc.nextFloat(); //taking the amount it wants to withdraw

		if(l<Balance+k)
		{
			System.out.println("withdraw Succesfull");
			Balance=Balance-l;
			System.out.println("Balance :"+Balance);	//change in the balance
		}
		else
		{
			System.out.print("Overdraft limit reached");
		}
	}

	public void Interest() //no interest in current
	{
		System.out.println("Sorry,this is Current Account"); 
	}
	public void file()//creating file with the name of the account number
	{
		String fileName=String.valueOf(Acc_no);	

		try
		{
			FileOutputStream outs=new FileOutputStream(fileName);//creating binary file
			ObjectOutputStream oout=new ObjectOutputStream(outs);

			String ac="Acc_no :"+Acc_no+"\n";//taking account no. and balance
			String ba="Balance :"+Balance+"\n";
			

			
			byte[] a=ac.getBytes();
			byte[] b=ba.getBytes();
			
			

			oout.writeObject(a);
			oout.writeObject(b);
			

			oout.close();

			System.out.println("Data stored");
		}
		catch(IOException ex)
		{
			System.out.println("Error while writting file :"+fileName);
		}
	}	
	public void delete()
	{				//for deleting file

		try
		{
			File file=new File(String.valueOf(Acc_no));//account number is the file name

			if(file.delete())//deletion of file
			{
				System.out.println("File deleted");
			}
			else
			{
				System.out.println("Delete operation is failed");//incorrect file name
			}

		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
}
}

class Banking
{
	public static void main(String[] args)  //main class
	{
		Scanner sc=new Scanner(System.in);

		Account A1;
		
		int c;
		do{
		   System.out.println("Enter the type of Account you want to create"); //giving choices to the user
		   System.out.println("1 Savings Account");
		   System.out.println("2 Current Account");
		   int s=sc.nextInt();

	        if(s==1)//if pressed 1 the saving
		   {
			Saving S1=new Saving();
			A1=S1;
			A1.Create();
			A1.file();
		   }
		   else//pressed 2 than current
		   {
			Current C1=new Current();
			A1=C1;
			A1.Create();
			A1.file();
			
		   }
		   c=sc.nextInt();
		  }while(c==1);

		int r;
		do{
		
			System.out.println("1.Exit");           //giving choices or options to the user
			System.out.println("2.Account Detail");
			System.out.println("3.Deposite money");
			System.out.println("4.Withdraw money");
			System.out.println("5.Count  Intrest");
			r=sc.nextInt(); //taking the choice of the user
	

			switch(r) //passing the selected option in the switch cases
			{
			        case 1:
			        	break;
				
				case 2:
					A1.Detail(); //now funtion detail will be executed
					A1.file();
					break;

				case 3:
					A1.Deposit(); //now function deposit will be executed
					A1.file();
					break;

				case 4:
					System.out.println("all the things must be greter than 0,bank is not responsible for wrong input");
					A1.Withdraw();//withdraw function
					A1.file();
					break;

				case 5:
					System.out.println("enter valid interestrate greater than 0");
					A1.Interest();//interest function
					A1.file();
					break;

			}
		}while(r!=1);
	}
	
} 