/*Assingment-8 Q-2*/

import java.nio.file.*;
import java.io.*;
import java.util.*;

class filecopy //main class file copy
{
private static final String Path="newfile";//taking in a path,by default it is created with this name

	
	public static void main(String[] args)
	{
		Scanner sc=new Scanner(System.in); //scanner function for scanning the the inputs
		int choice;
		String name,q;//for rename purpose

		do{		//using infinite do-while loop
			System.out.println("....................................................");//choices given to the user
			System.out.println("1.Count number of lines,words & characters");
			System.out.println("2.Copy content");
			System.out.println("3.Rename a file");
			System.out.println("4.exit");
			System.out.println("....................................................");
			System.out.print("enter choice:");			
			choice=sc.nextInt();//scanning the given choices and taking in

			switch(choice)//passing the choices in the switch cases
			{
				case 1://case 1 of counting lines,words and characters
					System.out.print("File name(with extention if it is not txt) :");//asking fo filr name	
					name=sc.next();//scanning

					FileReader fReader;		//reading file						
					try{
					fReader = new FileReader(name);
           				BufferedReader reader = new BufferedReader(fReader);//using buffer to protect the data of the file
            				String cursor;  
            				String content = "";
            				int lines = 0;	//intialization of the line character and words.
            				int words = 0;
            				int chars = 0;
           					 while((cursor = reader.readLine()) != null)
     					        {
               						  lines += 1;//countiing line
	              					  content += cursor;
 		              				 String []_words = cursor.split(" ");
 		              				 for( String w : _words)//for counting words.
               					         {
                						  words++;        
                					 }
 
            					 }
				                 chars = content.length();
            					System.out.println(chars + " Characters");//now printing the counted characters words line
            					System.out.println(words + " words" ); 
						System.out.println(lines + " lines");
 
						}//using catch if the file name entere never exixts
						catch(FileNotFoundException ex) 							
						{
							System.out.println("File not Found");
						}
						catch(IOException ex)									
						{
							System.out.println("error,try again");
						}
	
					break;

				case 2://case 2 for copy the file
					File file=new File(Path);//making object of the file

					try{
						file.createNewFile();//creating anew file
						System.out.println("File created and name is 'copy'");
					}
					catch(IOException e)
					{
						System.out.println("File exists");//if entered valid name
					}

					System.out.print("Enter file from you want to copy:");//asking the filr name/source file name
					String source=sc.next();
					System.out.print("Enter file name  of copied file :");//new name
					String place=sc.next();
					
					try
					{
						FileInputStream f1=new FileInputStream(source);//takinthe source file
						FileOutputStream f2=new FileOutputStream(place);//taking in the new file

						for(int i=0;i!=-1;)//copying the content of the file
						{
							i=f1.read();
							f2.write((byte)i);
						}
					
						f1.close();
						f2.close();
						System.out.println("File copied..");
					}
					catch(Exception e)
					{
						System.out.println("Error");
					}
					

					break;

				case 3:
					System.out.print("Enter old Name :");//this to rename the file
					q=sc.next();
					File oldName=new File(q);//taking the exits file into the account
					System.out.print("Enter new Name :");//new name of the file
					name=sc.next();
					File newName=new File(name);
	
					if(oldName.renameTo(newName))
					{
						System.out.println("New name is:"+newName);//show the new name
					}
					else
					{
					System.out.println("Error");//if file doesnt exists						
					}
					break;
				case 4:
					break;//for exit
					
			}
		}while(choice!=4);//end of do while
	}

}