/*Assingment-8 Q-3*/

import java.util.*;
import java.io.*;

class Engine//class engine
{
	Scanner sc=new Scanner(System.in);//scanner function

	public String type;//declaration of variables
	public double hp;//horse power
	public int torque;//torque
	public String tp;//transmission type	

	public void E_detail() //engine details
	{
	
		System.out.print("Enter Engine type :");//engine type
		type=sc.next();
		System.out.print("Enter Horsepower :");//horsepower
		hp=sc.nextDouble();
		System.out.print("Enter Torque :");//torque
		torque=sc.nextInt();
		System.out.print("Enter transmission type :");//transmission typw
		tp=sc.next();
	}

	public void E_show()//showing engine details whis is entered above
	{
		System.out.println("Engine type       :"+type);
		System.out.println("Horsepower        :"+hp);
		System.out.println("Torque            :"+torque);
		System.out.println("Transmission type :"+tp);
	}
	
}

abstract class Vehicle//making abstract class vehicle
{
	Scanner sc=new Scanner(System.in);//scanner function

	Engine en=new Engine();//creating object of engine

	protected String company;//declaring variables
	protected String col; //for colour
	protected String model;	//name

	public void V_Data()//vehicle data
	{
	
		System.out.print("Enter Vahicle Company :");//company
		company=sc.next();
		System.out.print("Enter Your Vahile model no. :");//model number
		model=sc.next();
		System.out.print("Enter your prefered vahicle colour :");//colour
		col=sc.next();
		en.E_detail();
	}
	
	public void V_show()//showing the entered data
	{
		System.out.println("Company :"+company);
		System.out.println("Car Name :"+model);
		System.out.println("Vahicle Colour :"+col);
		en.E_show();
	}

	abstract public void data();//abstract  declarations

	abstract public void show();//to show data

	abstract public void file();//for filr handeling

	abstract public void update();//to update the existing file

	public void delete()//for deleting the file
	{
		try
		{

			String fname;//file name 

			System.out.print("Enter File name you want to remove :");//the existing file name
			fname=sc.next();//taking in the file

			File file=new File(fname);//creating the object of the file

			if(file.delete())//if selected delete
			{
				System.out.println("Data deleted");//showing this if deleted
			}
			else
			{
				System.out.println("Delete operation failed");
			}
		}
		catch(Exception e)
		{
			System.out.println("Delete operation failed,check file name");//if file name entered wrong
		}

	}
}

class TwoWheeler extends Vehicle//inheritance of vehicle into two wheelers
{
	Scanner sc=new Scanner(System.in);//scanner function

	protected float cc;//taking different variables of the two wheelers
	protected String D_date;//delivery date
	protected String ac;//for more feautures

	public void data()
	{//taking data from the user for two wheelers
	
		V_Data();
		System.out.print("Enter CC :");//cc
		cc=sc.nextFloat();//cc
		System.out.print("Enter delivery date which you want :");
		D_date=sc.next();//delivery dates
		System.out.print("Any features/specifications you want :");
		ac=sc.next();//other features
	}

	public void show()//showing the user entered data
	{
		V_show();
		System.out.println("CC :"+cc);
		System.out.println("D_O_D :"+D_date);
		System.out.println("Specification :"+ac);
	}
	
	public void update()//updation of the enetered data
	{
		int nd;//new updated data variable as a choice

		System.out.println("1.Change Colour");
		System.out.println("2.Change delivery date");
		nd=sc.nextInt();

		switch(nd)//entered choices
		{
			case 1:
				System.out.print("New colour :");
				col=sc.next();
				break;//colour

			case 2:
				System.out.print("New delivery date :");
				D_date=sc.next();
				break;//new delivery date
		}		
	}

	Engine en=new Engine();//making object of engine

	public void file()
	{
	

		try
		{
		
			FileOutputStream outs=new FileOutputStream(model);//for binary file
			ObjectOutputStream out=new ObjectOutputStream(outs);

			String co="Comapny :"+company+"\n";
			String na="Model no . :"+model+"\n";
			String ca="Vahicle colour :"+col+"\n";
			String ty="Engine type :"+en.type+"\n";
			String ha="horsepower :"+en.hp+"\n";
			String to="Torque :"+en.torque+"\n";
			String ta="Transmission type :"+en.tp+"\n";
			String cb="CC :"+cc+"\n";
			String da="Delivery date :"+D_date+"\n";
			String aa="Specification :"+ac+"\n";

			byte[] a=co.getBytes();
			byte[] b=na.getBytes();
			byte[] c=ca.getBytes();
			byte[] d=ty.getBytes();
			byte[] e=ha.getBytes();
			byte[] f=to.getBytes();
			byte[] g=ta.getBytes();
			byte[] h=cb.getBytes();
			byte[] i=da.getBytes();
			byte[] j=aa.getBytes();

			out.writeObject(a);
			out.writeObject(b);
			out.writeObject(c);
			out.writeObject(d);
			out.writeObject(e);
			out.writeObject(f);
			out.writeObject(g);
			out.writeObject(h);
			out.writeObject(i);
			out.writeObject(j);

			out.close();

			System.out.println("Data Stored");//successfully stored data
		}
		catch(IOException ex)//if exception occured
		{
			System.out.println("Error while writting a file :"+model);
		}
			
	}

}

abstract class FourWheeler extends Vehicle//making four wheeler class of vehicle
{
	
	protected float c1;//variables declared
	protected String D_Date;

	public void F_data()
	{
	
		V_Data();//taking user data
		System.out.print("Enter CC :");
		c1=sc.nextFloat();
		System.out.print("Enter delivery date which you want :");
		D_Date=sc.next(); 
	}
	
	public void F_show()//showing user enetred data
	{
		V_show();
		System.out.println("CC :"+c1);
		System.out.println("D_O_D :"+D_Date);
	}

	public void update()//updation of vehicle
	{
		int nd;//variable for choice

		System.out.println("1-->Change Colour");
		System.out.println("2-->Change delivery date");
		nd=sc.nextInt();

		switch(nd)//entered choice
		{
			case 1:
				System.out.print("New colour :");
				col=sc.next();
				break;//for colour
			case 2:
				System.out.print("New delivery date :");
				D_Date=sc.next();
				break;//for new dates
		}
	}
 
}

	
class P_car extends FourWheeler//if private car
{
	Scanner sc=new Scanner(System.in);
	
	protected String C_type;//declaring variables
	protected String Airbag;

	public void data()//taking data
	{
	
		F_data();
		System.out.print("Enter your car type(Seaden / Mini) :");
		C_type=sc.next();//type of vehicle
		System.out.print("Do you want Airbag in your car(yes/no) ??");
		Airbag=sc.next();//air bags 
	}

	public void show()//showing the user entered data
	{
		F_show();
		System.out.println("Car type :"+C_type);
		System.out.println("Airbag   :"+Airbag);
	}

	Engine en=new Engine();//new object of engine

	public void file()
	{
		
		try
		{
		
			FileOutputStream outs=new FileOutputStream(model);
			ObjectOutputStream out=new ObjectOutputStream(outs);//for binary file

			String co="Comapny :"+company+"\n";
			String na="Car name :"+model+"\n";
			String ca="Vahicle colour :"+col+"\n";
			String ty="Engine type :"+en.type+"\n";
			String ha="horsepower :"+en.hp+"\n";
			String to="Torque :"+en.torque+"\n";
			String ta="Transmission type :"+en.tp+"\n";
			String cb="CC :"+c1+"\n";
			String da="Delivery date :"+D_Date+"\n";
			String aa="Car type :"+C_type+"\n";
			String ai="Airbag :"+Airbag+"\n";

			byte[] a=co.getBytes();
			byte[] b=na.getBytes();
			byte[] c=ca.getBytes();
			byte[] d=ty.getBytes();
			byte[] e=ha.getBytes();
			byte[] f=to.getBytes();
			byte[] g=ta.getBytes();
			byte[] h=cb.getBytes();
			byte[] i=da.getBytes();
			byte[] j=aa.getBytes();
			byte[] k=ai.getBytes();

			out.writeObject(a);
			out.writeObject(b);
			out.writeObject(c);
			out.writeObject(d);
			out.writeObject(e);
			out.writeObject(f);
			out.writeObject(g);
			out.writeObject(h);
			out.writeObject(i);
			out.writeObject(j);
			out.writeObject(k);

			out.close();

			System.out.println("Data Stored");//sucessfully stored
		}
		catch(IOException ex)
		{
			System.out.println("Error while writting a file :"+model);//if entered incorrect
		}
	}
}


class C_car extends FourWheeler//commercial class car
{ 

	protected String Car_type;
	public void data()//entering user data
	{
	
		F_data();
		System.out.print("Enter your car type(Seaden / Mini) :");
		Car_type=sc.next();//type of car
	}

	public void show()//showing data
	{
		F_show();
		System.out.println("Car type :"+Car_type);
	}

	Engine en=new Engine();

	public void file()
	{

		try
		{
		
			FileOutputStream outs=new FileOutputStream(model);
			ObjectOutputStream out=new ObjectOutputStream(outs);//for binary file

			String co="Comapny :"+company+"\n";
			String na="Car name :"+model+"\n";
			String ca="Vahicle colour :"+col+"\n";
			String ty="Engine type :"+en.type+"\n";
			String ha="horsepower :"+en.hp+"\n";
			String to="Torque :"+en.torque+"\n";
			String ta="Transmission type :"+en.tp+"\n";
			String cb="CC :"+c1+"\n";
			String da="Delivery date :"+D_Date+"\n";
			String aa="Car type :"+Car_type+"\n";

			byte[] a=co.getBytes();
			byte[] b=na.getBytes();
			byte[] c=ca.getBytes();
			byte[] d=ty.getBytes();
			byte[] e=ha.getBytes();
			byte[] f=to.getBytes();
			byte[] g=ta.getBytes();
			byte[] h=cb.getBytes();
			byte[] i=da.getBytes();
			byte[] j=aa.getBytes();

			out.writeObject(a);
			out.writeObject(b);
			out.writeObject(c);
			out.writeObject(d);
			out.writeObject(e);
			out.writeObject(f);
			out.writeObject(g);
			out.writeObject(h);
			out.writeObject(i);
			out.writeObject(j);

			out.close();

			System.out.println("Data Stored");//sucessful
		}
		catch(IOException ex)
		{
			System.out.println("Error while writting a file :"+model);//incorrect
		}
	}	
	
}


class myvehicle//main class

{
	public static void main(String[] args)
	{
	Scanner sc=new Scanner(System.in);//scanner for taking the values

	int select,i,j;

	Vehicle V1;

	System.out.println("Vehicle type");
	System.out.println("1.four Wheeler");
	System.out.println("2.two Wheeler");
	i=sc.nextInt();	//taking user choices

	if(i==1)//selected four wheeler
	{	
		System.out.println("Select Car type...");//type/purpose
		System.out.println("1.Private Car");
		System.out.println("2.Cummercial Car");
		j=sc.nextInt();

		if(j==1)//if private is selected
		{
			P_car P1=new P_car();//created object of the class p_car and takin data from the user
			V1=P1;//private car 1
			V1.data();//data entering
			V1.file();//file created
			System.out.println("car name is file name");
		}
		else
		{
			C_car C1=new C_car();//commercial car and c_car is executed
			V1=C1;
			V1.data();//taking data
			V1.file();//file created
			System.out.println("car name is file name");
		}
	}
	else
	{
		TwoWheeler T1=new TwoWheeler();//if teo wheeler is selscted
		V1=T1;//taking data from the user and file is created
		V1.data();
		V1.file();
		System.out.println("model no. is file name");
	}

	do{

		System.out.println("-------------------------------------------");
		System.out.println("1.Vehicle detail");
		System.out.println("2.Update detail");
		System.out.println("3.Delete detail");
		System.out.println("4.exit");
		System.out.println("model no.(2 wheeler) and car name(4 wheeler) is file name");
		System.out.println("-------------------------------------------");
		System.out.print("enter a choice:");
		select=sc.nextInt();//taking choices

		switch(select)//choice 
		{
				
			case 1:
				V1.show();//vehicle details
				break;

			case 2:
				V1.update();//update details
				V1.file();//impplementing in file
				break;

			case 3:
				V1.delete();//delete
				break;
			case 4:
				break;
		}

	}while(select !=4);	

	}
}










